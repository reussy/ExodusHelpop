package com.reussy.helpop.commands;

import com.reussy.helpop.ExodusHelpop;
import com.reussy.helpop.filemanager.FileManager;
import com.reussy.helpop.menus.StaffMenu;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StaffCommand implements CommandExecutor {

    private final ExodusHelpop plugin;
    FileManager FManager = new FileManager();

    public StaffCommand(ExodusHelpop plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender instanceof Player)) {

            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', FManager.getLanguage().getString("No-Console")
                    .replace("%prefix%", FManager.PX)));
            return false;
        }

        if (!sender.hasPermission("helpop.command.staff")) {

            sender.sendMessage(org.bukkit.ChatColor.translateAlternateColorCodes('&', FManager.getLanguage().getString("Insufficient-Permission")
                    .replace("%prefix%", FManager.PX)));
            return false;
        }

        if (cmd.getName().equalsIgnoreCase("helpop-staff")) {

            Player P = (Player) sender;
            StaffMenu Staff = new StaffMenu(plugin);
            Staff.Menu(P);
        }
        return false;
    }
}
