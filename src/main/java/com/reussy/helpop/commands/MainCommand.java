package com.reussy.helpop.commands;

import com.reussy.helpop.ExodusHelpop;
import com.reussy.helpop.filemanager.FileManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MainCommand implements CommandExecutor {

    private final ExodusHelpop plugin;
    FileManager FManager = new FileManager();

    public MainCommand(ExodusHelpop plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!sender.hasPermission("helpop.command.main")) {

            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', FManager.getLanguage().getString("Insufficient-Permission")
                    .replace("%prefix%", FManager.PX)));
            return false;
        }

        if (args.length < 1) {

            for (String Help : FManager.getLanguage().getStringList("Help")) {

                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Help));
            }
            return true;

        }

        if ("help".equals(args[0])) {

            for (String Help : FManager.getLanguage().getStringList("Help")) {

                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Help));
            }

        } else if ("reload".equals(args[0])) {
            plugin.reloadConfig();
            FManager.reloadLanguage();
            FManager.reloadMenus();

            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', FManager.getLanguage().getString("Reload-Message")
                    .replace("%prefix%", FManager.PX)));
        } else {

            for (String Help : FManager.getLanguage().getStringList("Help")) {

                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Help));
            }
        }

        return false;
    }
}
