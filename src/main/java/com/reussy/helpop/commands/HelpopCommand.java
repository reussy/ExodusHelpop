package com.reussy.helpop.commands;

import com.reussy.helpop.ExodusHelpop;
import com.reussy.helpop.filemanager.FileManager;
import com.reussy.helpop.utils.XSound;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class HelpopCommand implements CommandExecutor {

    private final ExodusHelpop plugin;
    public HashMap<String, Long> time = new HashMap<>();
    FileManager FManager = new FileManager();

    public HelpopCommand(ExodusHelpop plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender instanceof Player)) {

            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', FManager.getLanguage().getString("No-Console")
                    .replace("%prefix%", FManager.PX)));
            return false;
        }

        Player P = (Player) sender;
        int Time = plugin.getConfig().getInt("Time-Cooldown");

        if (!sender.hasPermission("helpop.command.send")) {

            P.sendMessage(ChatColor.translateAlternateColorCodes('&', FManager.getLanguage().getString("Insufficient-Permission")
                    .replace("%prefix%", FManager.PX)));
            return false;
        }

        if (args.length < 1) {

            P.sendMessage(ChatColor.translateAlternateColorCodes('&', FManager.getLanguage().getString("Invalid-Args")));
            return false;
        }

        if (time.containsKey(P.getName())) {

            long Left = ((time.get(P.getName()) / 1000) + Time) - (System.currentTimeMillis() / 1000);

            if (Left > 0) {

                P.sendMessage(ChatColor.translateAlternateColorCodes('&', FManager.getLanguage().getString("Cooldown-Help")
                        .replace("%seconds%", String.valueOf(Left))));
                return false;
            }
        }

        StringBuilder message = new StringBuilder();
        for (String arguments : args) {
            message.append(arguments).append(" ");

        }

        TextComponent Hover = new TextComponent(FManager.getLanguage().getString("HelpOP-Format"));
        Hover.setText(org.bukkit.ChatColor.translateAlternateColorCodes('&', FManager.getLanguage().getString("HelpOP-Format").replace("%player%", P.getName()).replace("%message%", message)));
        Hover.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, plugin.getConfig().getString("Hover-Command").replace("%player%", P.getName())));
        Hover.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, (new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', FManager.getLanguage().getString("Hover-Message")
                .replace("%player%", P.getName()))).create())));

        P.sendMessage(ChatColor.translateAlternateColorCodes('&', FManager.getLanguage().getString("HelpOP-Sended")));
        if (plugin.getConfig().getBoolean("Sounds.Sound-Player"))
            P.playSound(P.getLocation(), XSound.valueOf(plugin.getConfig().getString("Sounds.HelpOP-Player")).parseSound(), plugin.getConfig().getInt("Sounds.Volume"), plugin.getConfig().getInt("Sounds.Pitch"));

        if (!P.hasPermission("helpop.bypass.cooldown")) time.put(P.getName(), System.currentTimeMillis());

        //for each staff, send message
        for (Player S : Bukkit.getOnlinePlayers()) {

            if (S.hasPermission("helpop.action.receive")) {

                if (plugin.getConfig().getBoolean("Sounds.Sound-Staff"))
                    S.playSound(S.getLocation(), XSound.valueOf(plugin.getConfig().getString("Sounds.HelpOP-Staff")).parseSound(), plugin.getConfig().getInt("Sounds.Volume"), plugin.getConfig().getInt("Sounds.Pitch"));
                S.spigot().sendMessage(Hover);
            }
        }

        return false;
    }
}
