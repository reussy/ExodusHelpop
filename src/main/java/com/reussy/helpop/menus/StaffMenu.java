package com.reussy.helpop.menus;

import com.reussy.helpop.ExodusHelpop;
import com.reussy.helpop.filemanager.FileManager;
import com.reussy.helpop.utils.ItemCreator;
import com.reussy.helpop.utils.XMaterial;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class StaffMenu {

    private final ExodusHelpop plugin;
    FileManager FManager = new FileManager();
    ItemCreator createItem = new ItemCreator();

    public StaffMenu(ExodusHelpop plugin) {
        this.plugin = plugin;
    }

    public void Menu(Player P) {

        Inventory GUI = Bukkit.createInventory(null, 54, ChatColor.translateAlternateColorCodes('&', FManager.getMenus().getString("GUI-Staff.Title-GUI")));

        ItemStack Glass = createItem.normalItem(XMaterial.BLACK_STAINED_GLASS_PANE, 1,
                ChatColor.translateAlternateColorCodes('&', "&7"), null);

        for (int I = 45; I <= 53; I++) GUI.setItem(I, Glass);

        int Slot = 0;

        for (Player S : Bukkit.getOnlinePlayers()) {

            if (S.hasPermission("helpop.gui.staff")) {

                String Name = ChatColor.translateAlternateColorCodes('&', FManager.getMenus().getString("GUI-Staff.Staff-Head"));
                Name = PlaceholderAPI.setPlaceholders(S, Name);
                List<String> HeadLore = new ArrayList<>();

                for (String LoreHead : FManager.getMenus().getStringList("GUI-Staff.Staff-Lore")) {

                    HeadLore.add(ChatColor.translateAlternateColorCodes('&', LoreHead));
                }

                HeadLore = PlaceholderAPI.setPlaceholders(S, HeadLore);

                ItemStack Staff = createItem.headItem(XMaterial.PLAYER_HEAD, 1, S.getName(), Name, HeadLore);

                GUI.setItem(Slot, Staff);

                Slot++;

                if (Slot > 44) break;
            }
        }

        List<String> CloseLore = new ArrayList<>();

        for (String LoreClose : FManager.getMenus().getStringList("GUI-Staff.Close-Lore")) {

            CloseLore.add(ChatColor.translateAlternateColorCodes('&', LoreClose));
        }

        ItemStack Close = createItem.normalItem(XMaterial.valueOf(FManager.getMenus().getString("GUI-Staff.Close-Material")), 1,
                ChatColor.translateAlternateColorCodes('&', FManager.getMenus().getString("GUI-Staff.Close")), CloseLore);

        GUI.setItem(49, Close);

        P.openInventory(GUI);
    }
}
