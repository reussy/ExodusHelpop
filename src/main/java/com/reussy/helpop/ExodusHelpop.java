package com.reussy.helpop;

import com.reussy.helpop.commands.HelpopCommand;
import com.reussy.helpop.commands.MainCommand;
import com.reussy.helpop.commands.StaffCommand;
import com.reussy.helpop.events.StaffMenuEvent;
import com.reussy.helpop.filemanager.FileManager;
import com.reussy.helpop.utils.PluginUpdates;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public final class ExodusHelpop extends JavaPlugin {

    PluginDescriptionFile Description = this.getDescription();
    String version = Description.getVersion();

    @Override
    public void onEnable() {

        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m------------------------------------------------"));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7"));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7> &c&lExodus HelpOP &8| &aEnabled "));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7"));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7> &fAuthor: &8reussy"));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7> &fVersion: &8" + this.version));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7"));
        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {

            Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&lExodus HelpOP &8| hooked to PlaceholderAPI"));

        }

        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7"));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m------------------------------------------------"));

        Commands();
        Events();
        FilesManager();

    }

    @Override
    public void onDisable() {

        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m------------------------------------------------"));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7"));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7> &c&lExodus HelpOP &8| &cDisabled "));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&7"));
        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m------------------------------------------------"));
    }

    public void Commands() {

        this.getCommand("helpop-admin").setExecutor(new MainCommand(this));
        this.getCommand("helpop-staff").setExecutor(new StaffCommand(this));
        this.getCommand("helpop").setExecutor(new HelpopCommand(this));
    }

    public void Events() {

        Bukkit.getPluginManager().registerEvents(new StaffMenuEvent(), (this));
    }

    public void FilesManager() {

        FileManager FManager = new FileManager();

        FManager.createConfig();
        FManager.createLanguage();
        FManager.createMenus();
    }
}