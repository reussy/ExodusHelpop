package com.reussy.helpop.events;

import com.reussy.helpop.filemanager.FileManager;
import com.reussy.helpop.utils.XMaterial;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class StaffMenuEvent implements Listener {

    FileManager FManager = new FileManager();

    @EventHandler
    public void InteractMenu(InventoryClickEvent e) {

        if (e.getView().getTitle().equals(ChatColor.translateAlternateColorCodes('&', FManager.getMenus().getString("GUI-Staff.Title-GUI")))) {

            if (e.getCurrentItem() == null) return;

            if (e.getCurrentItem() == null || e.getCurrentItem().getType() == XMaterial.AIR.parseMaterial()) return;

            e.setCancelled(true);

            Player S = (Player) e.getWhoClicked();

            if (e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', FManager.getMenus().getString("GUI-Staff.Close"))))
                S.closeInventory();

        }
    }
}
