package com.reussy.helpop.utils;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Consumer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

public class PluginUpdates {

    private final JavaPlugin plugin;
    private final int ID;

    public PluginUpdates(JavaPlugin plugin, int ID) {

        this.plugin = plugin;
        this.ID = ID;
    }

    public void getVersion(final Consumer<String> consumer) {

        Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> {

            try (InputStream inputStream = new URL("https://api.spigotmc.org/legacy/update.php?resource=" + this.ID).openStream(); Scanner scanner = new Scanner(inputStream)) {
                if (scanner.hasNext()) {
                    consumer.accept(scanner.next());
                }
            } catch (IOException exception) {

                this.plugin.getLogger().info("&cAn error occurred while getting updates from Exodus HelpOP &4" + exception.getCause());
            }
        });
    }
}
