package com.reussy.helpop.filemanager;

import com.reussy.helpop.ExodusHelpop;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class FileManager {

    private final ExodusHelpop plugin = ExodusHelpop.getPlugin(ExodusHelpop.class);
    public String PX = ChatColor.translateAlternateColorCodes('&', getLanguage().getString("Plugin-Prefix"));
    private final File Config = new File(plugin.getDataFolder(), "config.yml");
    private File LanguageFile = new File(plugin.getDataFolder(), "language.yml");
    private File MenusFile = new File(plugin.getDataFolder(), "menus.yml");
    private FileConfiguration Menus = YamlConfiguration.loadConfiguration(MenusFile);
    private FileConfiguration Language = YamlConfiguration.loadConfiguration(LanguageFile);

    public void createConfig() {

        if (!Config.exists()) {

            plugin.saveResource("config.yml", false);

        }
    }

    public void createLanguage() {

        if (!LanguageFile.exists()) {

            plugin.saveResource("language.yml", false);
        }
    }

    public FileConfiguration getLanguage() {

        if (Language == null) {
            reloadLanguage();
        }

        return Language;
    }

    public void reloadLanguage() {

        if (LanguageFile == null) {

            LanguageFile = new File(plugin.getDataFolder(), "language.yml");

        }

        Language = YamlConfiguration.loadConfiguration(LanguageFile);
        Reader defConfigStream;

        defConfigStream = new InputStreamReader(Objects.requireNonNull(plugin.getResource("language.yml")), StandardCharsets.UTF_8);

        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
        Language.setDefaults(defConfig);


    }

    public void registerMessageFile() {

        LanguageFile = new File(plugin.getDataFolder(), "messages.yml");

        LanguageFile.exists();

        this.getLanguage().options().copyDefaults(true);
        plugin.saveDefaultConfig();
        saveMessages();
    }

    public void saveMessages() {

        try {

            Language.save(LanguageFile);
            plugin.saveDefaultConfig();

        } catch (IOException e) {

            e.printStackTrace();

        }
    }

    public void createMenus() {

        if (!MenusFile.exists()) {

            plugin.saveResource("menus.yml", false);
        }
    }

    public FileConfiguration getMenus() {

        if (Menus == null) {
            reloadMenus();
        }

        return Menus;
    }

    public void reloadMenus() {

        if (MenusFile == null) {

            MenusFile = new File(plugin.getDataFolder(), "menus.yml");

        }

        Menus = YamlConfiguration.loadConfiguration(MenusFile);
        Reader defConfigStream;

        defConfigStream = new InputStreamReader(Objects.requireNonNull(plugin.getResource("menus.yml")), StandardCharsets.UTF_8);

        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
        Menus.setDefaults(defConfig);


    }

    public void registerMenusFile() {

        LanguageFile = new File(plugin.getDataFolder(), "menus.yml");

        LanguageFile.exists();

        this.getMenus().options().copyDefaults(true);
        plugin.saveDefaultConfig();
        saveMenus();
    }

    public void saveMenus() {

        try {

            Menus.save(MenusFile);
            plugin.saveDefaultConfig();

        } catch (IOException e) {

            e.printStackTrace();

        }
    }

}
